import logo from "./logo.svg";
import { Component } from "react";
import "./App.css";
import Switch from "./Components/Switch";
import Lock from "./Components/Lock";

class App extends Component {
  state = {
    showLogo: true,
    locked: false,
  };

  showLogo = () => this.setState({ showLogo: !this.state.showLogo });

  locked = () => this.setState({ locked: !this.state.locked });

  render() {
    return (
      <div className="App">
        <header className="App-header">
          {this.state.showLogo && (
            <img src={logo} className="App-logo" alt="logo" />
          )}

          <Switch
            action={this.showLogo}
            showLogo={this.state.showLogo}
            locked={this.state.locked}
          />
          <Lock action={this.locked} locked={this.state.locked} />
        </header>
      </div>
    );
  }
}

export default App;
